package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.RoomService;
import cz.uhk.kppro.roomorg.business.UserService;
import cz.uhk.kppro.roomorg.model.Room;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.util.UserUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

/***
 *  Provides rest access for {@link User} and {@link Room} operations.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserinfoController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoomService roomService;

    /**
     * @return info about current user.
     */
    @GetMapping("/me")
    public ResponseEntity<User> currentUser() {
        return ok(userService.findByUsername(UserUtil.currUser()).orElseThrow());
    }

    /**
     * Not used for now..
     *
     * @see RoomService#createRoom(Room)
     */
    @PostMapping("/createRoom")
    public ResponseEntity<User> createRoom(@RequestBody Room room) {
        return ok(roomService.createRoom(room));
    }

    /**
     * Not used for now..
     *
     * @see RoomService#changeRoom(RoomChangeRequest)
     */
    @PostMapping("/changeRoom")
    public ResponseEntity<User> changeRoom(@RequestBody RoomChangeRequest roomChangeRequest) {
        try {
            return ok(roomService.changeRoom(roomChangeRequest));
        } catch (IllegalAccessException e) {
            return status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * DTO that holds basic info about change request from req context.
     */
    @Data
    @AllArgsConstructor
    public static class RoomChangeRequest {
        private String roomName;
        private String roomKey;
    }
}
