package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.UserService;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.util.UserUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

/***
 *  Provides rest access for signing, login and logout of user.
 *  @see User and {@link UserService}
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@CommonsLog
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    /**
     * Registers user.
     */
    @PostMapping("/register")
    public ResponseEntity<Map<String, String>> register(@RequestBody AuthenticationRequest data) {
        userService.register(data);
        return authenticate(data);
    }

    /**
     * Login user.
     */
    @PostMapping("/auth")
    public ResponseEntity<Map<String, String>> authenticate(@RequestBody AuthenticationRequest data) {

        try {
            Map<String, String> model = userService.authenticate(data);
            return ok(model);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied", e);
        }
    }

    /**
     * Logout user.
     */
    @GetMapping("/logout")
    public ResponseEntity<Object> logout() {
        userService.logout(UserUtil.currUser());
        return ok().build();
    }

    /**
     * DTO that holds basic info from req.
     */
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationRequest implements Serializable {
        private String username;
        private String password;
    }

}
