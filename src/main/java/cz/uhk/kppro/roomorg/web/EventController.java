package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.EventService;
import cz.uhk.kppro.roomorg.dao.UserRepository;
import cz.uhk.kppro.roomorg.model.Event;
import cz.uhk.kppro.roomorg.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

/***
 *  Provides rest access for {@link Event} operations.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@RequestMapping("event")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EventController {

    @Autowired
    private EventService eventService;
    @Autowired
    private UserRepository userRepository;


    /**
     * @see EventService#getList().
     */
    @GetMapping("/all")
    public ResponseEntity<List<Event>> get() {
        return ok(eventService.getList());
    }

    /**
     * @see EventService#getById(int).
     */
    @GetMapping("/get/{id}")
    public ResponseEntity<Event> get(@PathVariable int id) {
        Event byId = eventService.getById(id);
        if (byId == null) return noContent().build();
        return ok(byId);
    }

    /**
     * @see EventService#delete(int).
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        eventService.delete(id);
        return ok("Removed " + id);

    }

    /**
     * @see #update(Event)
     */
    @PostMapping("/store")
    public ResponseEntity<Event> save(@RequestBody Event event) {
        if (event.getId() == null) event.setCreated(LocalDateTime.now());
        return update(event);
    }

    /**
     * @see EventService#store(Event).
     */
    @PostMapping("/update")
    public ResponseEntity<Event> update(@RequestBody Event event) {
        userRepository.findByUsername(UserUtil.currUser()).ifPresent(i -> event.setAuthorId(i.getId()));
        eventService.store(event);
        return ok(event);
    }


}
