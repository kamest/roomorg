package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.MessageService;
import cz.uhk.kppro.roomorg.dao.UserRepository;
import cz.uhk.kppro.roomorg.model.Message;
import cz.uhk.kppro.roomorg.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

/***
 *  Provides rest access for {@link Message} operations.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@RequestMapping("messages")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private UserRepository userRepository;

    /**
     * @see MessageService#getList().
     */
    @GetMapping("/all")
    public ResponseEntity<List<Message>> get() {
        return ok(messageService.getList());
    }

    /**
     * @see MessageService#getById(int).
     */
    @GetMapping("/get/{id}")
    public ResponseEntity<Message> get(@PathVariable int id) {
        Message byId = messageService.getById(id);
        if (byId == null) return noContent().build();
        return ok(byId);
    }

    /**
     * @see MessageService#delete(int).
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        messageService.delete(id);
        return ok("Removed " + id);
    }

    /**
     * @see #update(Message)
     */
    @PostMapping("/store")
    public ResponseEntity<Message> save(@RequestBody Message message) {
        if (message.getId() == null) message.setCreated(LocalDateTime.now());
        return update(message);
    }

    /**
     * @see MessageService#store(Message).
     */
    @PostMapping("/update")
    public ResponseEntity<Message> update(@RequestBody Message message) {
        userRepository.findByUsername(UserUtil.currUser()).ifPresent(message::setAuthor);
        messageService.store(message);
        return ok(message);
    }


}
