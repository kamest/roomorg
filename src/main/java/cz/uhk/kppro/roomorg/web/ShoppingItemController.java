package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.ShoppingItemService;
import cz.uhk.kppro.roomorg.business.UserService;
import cz.uhk.kppro.roomorg.model.ShoppingItem;
import cz.uhk.kppro.roomorg.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

/***
 *  Provides rest access for {@link ShoppingItem} operations.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@RequestMapping("shoppingItem")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ShoppingItemController {

    @Autowired
    private ShoppingItemService siService;
    @Autowired
    private UserService userService;

    /**
     * @see ShoppingItemService#getList().
     */
    @GetMapping("/all")
    public ResponseEntity<List<ShoppingItem>> get() {
        return ok(siService.getList());
    }

    /**
     * @see ShoppingItemService#getById(int).
     */
    @GetMapping("/get/{id}")
    public ResponseEntity<ShoppingItem> get(@PathVariable int id) {
        ShoppingItem byId = siService.getById(id);
        if (byId == null) return noContent().build();
        return ok(byId);
    }

    /**
     * @see ShoppingItemService#delete(int).
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        siService.delete(id);
        return ok("Removed " + id);

    }

    /**
     * @see #update(ShoppingItem)
     */

    @PostMapping("/store")
    public ResponseEntity<ShoppingItem> save(@RequestBody ShoppingItem shoppingItem) {
        if (shoppingItem.getId() == null) shoppingItem.setCreated(LocalDateTime.now());
        return update(shoppingItem);
    }

    /**
     * @see ShoppingItemService#store(ShoppingItem).
     */
    @PostMapping("/update")
    public ResponseEntity<ShoppingItem> update(@RequestBody ShoppingItem shoppingItem) {
        userService.findByUsername(UserUtil.currUser()).ifPresent(shoppingItem::setAuthor);
        siService.store(shoppingItem);
        return ok(shoppingItem);
    }


}
