package cz.uhk.kppro.roomorg.web;

import cz.uhk.kppro.roomorg.business.GalleryService;
import cz.uhk.kppro.roomorg.dao.UserRepository;
import cz.uhk.kppro.roomorg.model.Photo;
import cz.uhk.kppro.roomorg.model.PhotoWithByteArray;
import cz.uhk.kppro.roomorg.model.PhotoWithMultipartFile;
import cz.uhk.kppro.roomorg.util.UserUtil;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

/***
 *  Provides rest access for {@link Photo} operations.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@RestController
@RequestMapping("gallery")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@CommonsLog
public class GalleryController {

    @Autowired
    private GalleryService galleryService;


    /**
     * Retrieve content of fs files into dto {@link PhotoWithByteArray}.
     *
     * @see GalleryService#getList().
     */
    @GetMapping("/all")
    public ResponseEntity<List<PhotoWithByteArray>> get() {
        return ok(
                galleryService
                        .getList()
                        .stream()
                        .map(i -> {
                            try {
                                return new PhotoWithByteArray(i, IOUtils.toByteArray(new FileSystemResource(i.getPath()).getInputStream()));
                            } catch (IOException e) {
                                log.error(e.getMessage(), e);
                                return new PhotoWithByteArray(i, null);
                            }
                        })
                        .collect(Collectors.toList())
        );
    }

    /**
     * Retrieve content of fs file into dto {@link PhotoWithByteArray}.
     *
     * @see GalleryService#getById(int).
     */
    @GetMapping("/get/{id}")
    public ResponseEntity<PhotoWithByteArray> get(@PathVariable int id) throws IOException {
        PhotoWithByteArray byId = galleryService.getById(id);
        if (byId == null) return noContent().build();
        return ok(byId);
    }

    /**
     * @see GalleryService#delete(int).
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        Photo byId = galleryService.getById(id);
        Assert.notNull(byId, "Photo dto cannot be null!");
        boolean deleted = new File(byId.getPath()).delete();
        Assert.isTrue(deleted, "Photo file cannot be null!");
        galleryService.delete(id);
        return ok("Removed " + id);

    }

    /**
     * @see #update(PhotoWithMultipartFile)
     */
    @PostMapping(path = "/store", consumes = "multipart/form-data")
    public ResponseEntity<PhotoWithByteArray> save(@ModelAttribute PhotoWithMultipartFile photo) throws IOException {
        if (photo.getId() == null) photo.setCreated(LocalDateTime.now());
        return update(photo);
    }

    /**
     * Stores photo content to FileSystem
     * and after that stores photo to db with path of fs file.
     *
     * @see GalleryService#store(PhotoWithMultipartFile, InputStream, String).
     */
    @PostMapping(path = "/update", consumes = "multipart/form-data")
    public ResponseEntity<PhotoWithByteArray> update(@ModelAttribute PhotoWithMultipartFile photo) throws IOException {

        MultipartFile multipartFile = photo.getMultipartFile();
        Assert.notNull(multipartFile, "Photo cannot be null!");

        Photo photoDto = galleryService.store(photo, multipartFile.getInputStream(), multipartFile.getOriginalFilename());

        return ok(new PhotoWithByteArray(photoDto, IOUtils.toByteArray(new FileSystemResource(photoDto.getPath()).getInputStream())));
    }


}
