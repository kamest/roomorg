package cz.uhk.kppro.roomorg.dao;

import cz.uhk.kppro.roomorg.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/***
 *  Basic jpa repository of {@link Room}
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Optional<Room> findByName(String name);

    boolean existsByName(String name);


}
