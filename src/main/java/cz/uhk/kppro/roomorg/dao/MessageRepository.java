package cz.uhk.kppro.roomorg.dao;

import cz.uhk.kppro.roomorg.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/***
 *  Basic jpa repository of {@link Message}
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
