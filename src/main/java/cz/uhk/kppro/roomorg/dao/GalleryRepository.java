package cz.uhk.kppro.roomorg.dao;

import cz.uhk.kppro.roomorg.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/***
 *  Basic jpa repository of {@link Photo}
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Repository
public interface GalleryRepository extends JpaRepository<Photo, Integer> {

}
