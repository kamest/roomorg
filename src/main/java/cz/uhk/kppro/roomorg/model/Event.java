package cz.uhk.kppro.roomorg.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/***
 *  Basic entity of event, event is used for planning activity in calendar.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Event implements IdProvider{

	public Event(String title, String description) {
		this.title = title;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Integer id;

	@Column
	private Integer authorId;

	@Column
	private String title;

	@Column
	private String description;

	@Column
	private LocalDateTime eventDate;
	@Column
	private LocalDateTime created;


}
