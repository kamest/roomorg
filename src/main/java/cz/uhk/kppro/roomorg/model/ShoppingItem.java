package cz.uhk.kppro.roomorg.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/***
 *  Basic entity of shoppingItem, is used for planning shop carts.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShoppingItem implements IdProvider{

	public ShoppingItem(String title, Double quantity) {
		this.title = title;
		this.quantity = quantity;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Integer id;

	@OneToOne
	@JoinColumn(name = "author_id")
	private User author;

	/**
	 * Name of item.
	 * ex. Milk
	 */
	@Column
	private String title;

	/**
	 * Quantity to buy.
	 * ex. 2 pieces, 3.52Kg
	 */
	@Column
	private Double quantity;

	@Column
	private LocalDateTime created;


}
