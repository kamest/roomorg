package cz.uhk.kppro.roomorg.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/***
 *  {@link Photo} with byte content - for sending to client .
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhotoWithByteArray extends Photo {

    public PhotoWithByteArray(Photo photo, byte[] content) {
        super(photo.getId(), photo.getAuthor(), photo.getTitle(), photo.getPath(), photo.getCreated());
        this.content = content;
    }

    private byte[] content;
}
