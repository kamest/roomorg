package cz.uhk.kppro.roomorg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/***
 *  Basic entity of photo, is used for gallery.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Photo implements IdProvider{

	public Photo(PhotoWithMultipartFile photo) {
		this.id = photo.getId();
		this.author = photo.getAuthor();
		this.title = photo.getTitle();
		this.path = photo.getPath();
		this.created = photo.getCreated();
	}
	public Photo(String title) {
		this.title = title;
	}
	public Photo(User author, String title) {
		this.author = author;
		this.title = title;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Integer id;

	@OneToOne
	@JoinColumn(name = "author_id")
	private User author;

	/**
	 * Description of photo.
	 */
	@Column
	private String title;

	/**
	 * Absolute path to file.
	 */
	@Column
	@JsonIgnore
	private String path;

	@Column
	private LocalDateTime created;


}
