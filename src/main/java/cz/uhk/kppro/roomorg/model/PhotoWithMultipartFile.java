package cz.uhk.kppro.roomorg.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/***
 *  {@link Photo} with {@link MultipartFile} - received from client.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhotoWithMultipartFile extends Photo {

    public PhotoWithMultipartFile(Photo photo, MultipartFile multipartFile) {
        super(photo.getId(), photo.getAuthor(), photo.getTitle(), photo.getPath(), photo.getCreated());
        this.multipartFile = multipartFile;
    }

    private MultipartFile multipartFile;
}
