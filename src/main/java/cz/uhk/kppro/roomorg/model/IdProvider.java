package cz.uhk.kppro.roomorg.model;

/***
 *  Basic id provider.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
public interface IdProvider {

    Integer getId();
}
