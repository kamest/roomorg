package cz.uhk.kppro.roomorg.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/***
 *  Basic entity of room, could be used for creation many rooms in system.
 *
 *  Each user could be signed into one room and content will be shown
 *  only that lies for the room.
 *
 *  In case:
 *  User A and B could share room A and see photos 1,2,3
 *  User C and D could share room B and dont see 1,2,3 photos ,
 *  but will see photos that they uploaded - 4,5,6
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Room implements IdProvider{

	/**
	 * Name of the room.
	 */
	@Column
	private String name;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Integer id;
	/**
	 * Md5 of passkey.
	 */
	@Column
	private String secret;
	/**
	 * Passkey - user must know for joining room.
	 */
	private String passKey;

	public Room(String name, String passKey) {
		this.name = name;
		this.passKey = passKey;
	}

}
