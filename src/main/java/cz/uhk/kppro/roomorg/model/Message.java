package cz.uhk.kppro.roomorg.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/***
 *  Basic entity of message, is used for chatting.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Message implements IdProvider{

	public Message(String content) {
		this.content = content;
	}
	public Message(User author,String content) {
		this.author = author;
		this.content = content;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private Integer id;

	@OneToOne
	@JoinColumn(name = "author_id")
	private User author;

	@Column
	private String content;

	@Column
	private LocalDateTime created;

}
