package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.MessageRepository;
import cz.uhk.kppro.roomorg.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/***
 *  Service that handles all operations on {@link Message}.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class MessageService {

    private final MessageRepository messageDao;

    @Transactional
    public List<Message> getList() {
        return messageDao.findAll();
    }

    @Transactional
    public Message getById(int id) {
        return messageDao.existsById(id) ? messageDao.getOne(id) : null;
    }

    @Transactional
    public void store(Message message) {
        messageDao.save(message);
    }

    @Transactional
    public void delete(Message message) {
        messageDao.delete(message);
    }

    @Transactional
    public void delete(int id) {
        messageDao.deleteById(id);
    }


}
