package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.RoomRepository;
import cz.uhk.kppro.roomorg.model.Room;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.util.UserUtil;
import cz.uhk.kppro.roomorg.web.UserinfoController;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/***
 *  Service that handles all operations on {@link Room}.
 *  Not used for now.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;
    private final UserService userService;

    @Value("${security.hash.salt}")
    private String salt;


    /**
     * Creates new room.
     */
    @Transactional
    public User createRoom(Room room) {

        String key = room.getPassKey();

        room.setSecret(md5(key));
        room.setPassKey(null);
        roomRepository.save(room);

        User user = getUser();
        user.setRoom(room);
        userService.save(user);
        return getUser();
    }

    /**
     * Changes room for user.
     */
    @Transactional
    public User changeRoom(UserinfoController.RoomChangeRequest roomChangeRequest) throws IllegalAccessException {

        Room room = roomRepository.existsByName(roomChangeRequest.getRoomName()) ? roomRepository.findByName(roomChangeRequest.getRoomName()).orElseThrow() : null;
        if (room == null) {
            throw new IllegalArgumentException();
        } else if (!Objects.equals(room.getSecret(), md5(roomChangeRequest.getRoomKey()))) {
            throw new IllegalAccessException();
        }

        User user = getUser();
        user.setRoom(room);
        userService.save(user);
        return getUser();

    }

    // util methods

    private User getUser() {
        return userService.findByUsername(UserUtil.currUser()).orElseThrow();
    }

    private String md5(String input) {
        String md5 = null;
        if (null == input) return null;
        input = input + salt;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }
}
