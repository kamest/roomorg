package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.ShoppingItemRepository;
import cz.uhk.kppro.roomorg.model.ShoppingItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/***
 *  Service that handles all operations on {@link ShoppingItem}.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class ShoppingItemService {

    private final ShoppingItemRepository shoppingItemRepository;

    /**
     * @return all {@link ShoppingItem}s that exists in system, do not filter for room for now.
     */
    @Transactional
    public List<ShoppingItem> getList() {
        return shoppingItemRepository.findAll();
    }

    /**
     * @return specific {@link ShoppingItem} by id that exists in system, do not filter for room for now.
     */
    @Transactional
    public ShoppingItem getById(int id) {
        return shoppingItemRepository.existsById(id) ? shoppingItemRepository.getOne(id) : null;
    }

    /**
     * Stores {@link ShoppingItem}.
     */
    @Transactional
    public void store(ShoppingItem shoppingItem) {
        shoppingItemRepository.save(shoppingItem);
    }

    /**
     * Removes specific {@link ShoppingItem}.
     */
    @Transactional
    public void delete(int id) {
        shoppingItemRepository.deleteById(id);
    }


}
