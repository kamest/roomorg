package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.EventRepository;
import cz.uhk.kppro.roomorg.model.Event;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/***
 *  Service that handles all operations on {@link Event}.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;

    /**
     * @return all {@link Event}s that exists in system, do not filter for room for now.
     */
    @Transactional
    public List<Event> getList() {
        return eventRepository.findAll();
    }

    /**
     * @return specific {@link Event} by id that exists in system, do not filter for room for now.
     */
    @Transactional
    public Event getById(int id) {
        return eventRepository.existsById(id) ? eventRepository.getOne(id) : null;
    }

    /**
     * Stores {@link Event}.
     */
    @Transactional
    public void store(Event event) {
        eventRepository.save(event);
    }

    /**
     * Removes specific {@link Event}.
     */
    @Transactional
    public void delete(int id) {
        eventRepository.deleteById(id);
    }


}
