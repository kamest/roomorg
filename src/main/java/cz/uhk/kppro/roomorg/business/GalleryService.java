package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.GalleryRepository;
import cz.uhk.kppro.roomorg.dao.UserRepository;
import cz.uhk.kppro.roomorg.model.Photo;
import cz.uhk.kppro.roomorg.model.PhotoWithByteArray;
import cz.uhk.kppro.roomorg.model.PhotoWithMultipartFile;
import cz.uhk.kppro.roomorg.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/***
 *  Service that handles all operations on {@link Photo}.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class GalleryService {

    private final GalleryRepository galleryDao;
    private final UserRepository userRepository;

    /**
     * Base path to store images.
     */
    @Value("${fs.origin.path}")
    private String originPath;

    /**
     * @return all {@link Photo}s that exists in system, do not filter for room for now.
     */
    @Transactional
    public List<Photo> getList() {
        return galleryDao.findAll();
    }

    /**
     * @return specific {@link Photo} by id that exists in system, do not filter for room for now.
     */
    @Transactional
    public PhotoWithByteArray getById(int id) {
        Photo photo = galleryDao.existsById(id) ? galleryDao.getOne(id) : null;
        return Optional
                .ofNullable(photo)
                .map(i-> {
                    try {
                        return new PhotoWithByteArray(i, IOUtils.toByteArray(new FileSystemResource(i.getPath()).getInputStream()));
                    } catch (IOException e) {
                        throw new IllegalArgumentException("File of photo : " + i.getId() +" must exist!");
                    }
                })
                .orElse(null);
    }

    /**
     * Stores {@link Photo}.
     */
    @Transactional
    public Photo store(PhotoWithMultipartFile photo, InputStream inputStream, String fileName) throws IOException {

        String absoluteFileName = originPath + fileName;
        File targetFile = new File(absoluteFileName);
        // rewrite file
        FileUtils.copyInputStreamToFile(inputStream, targetFile);

        userRepository.findByUsername(UserUtil.currUser()).ifPresent(photo::setAuthor);

        photo.setPath(absoluteFileName);
        Photo photoDto = new Photo(photo);
        photoDto = galleryDao.save(photoDto);
        return photoDto;
    }

    /**
     * Removes specific {@link Photo}.
     */
    @Transactional
    public void delete(int id) {
        galleryDao.deleteById(id);
    }


}
