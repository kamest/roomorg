package cz.uhk.kppro.roomorg.business;

import cz.uhk.kppro.roomorg.dao.UserRepository;
import cz.uhk.kppro.roomorg.model.Room;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.security.JwtTokenProvider;
import cz.uhk.kppro.roomorg.web.AuthenticationController;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/***
 *  Service that handles all operations on {@link User}.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2020
 **/
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final EntityManager entityManager;
    private final PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    /**
     * @return user by username - with room, without secrets.
     */
    @Transactional
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username).map(i -> {
            entityManager.detach(i);
            Optional<Room> room = Optional.ofNullable(i.getRoom());
            room.ifPresent(j -> {
                entityManager.detach(j);
                j.setSecret(null);
                i.setRoom(j);
            });
            i.setPassword(null);
            return i;
        });
    }

    /**
     * Stores user.
     */
    @Transactional
    public void save(User user) {
        Optional<User> optionalUser = findByUsernameWithSecrets(user.getUsername());
        optionalUser.ifPresent(i -> {
            user.setPassword(i.getPassword());
            Optional.ofNullable(user.getRoom())
                    .ifPresent(j ->
                            Optional.ofNullable(i.getRoom())
                                    .map(Room::getSecret)
                                    .ifPresent(j::setSecret)
                    );
        });
        userRepository.save(user);
    }

    /**
     * Registers new user
     */
    public void register(AuthenticationController.AuthenticationRequest data) {

        Optional<User> existingUser = userRepository.findByUsername(data.getUsername());
        if (existingUser.isPresent()) {
            throw new BadCredentialsException("User with this name already exists");
        }

        userRepository.save(
                new User(
                        data.getUsername(),
                        passwordEncoder.encode(data.getPassword()),
                        Collections.singletonList("ROLE_USER")
                )
        );
    }

    /**
     * Authenticate user and returns token for api.
     */
    public Map<String, String> authenticate(AuthenticationController.AuthenticationRequest data) throws AuthenticationException {

        String username = data.getUsername();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
        String token = jwtTokenProvider.createToken(
                username,
                userRepository
                        .findByUsername(username)
                        .orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found"))
                        .getRoles()

        );

        Map<String, String> args = new HashMap<>();
        args.put("username", username);
        args.put("token", token);
        return args;
    }

    /**
     * Logout - discards token for user.
     */
    public void logout(String username) {
        jwtTokenProvider.disableToken(username);
    }

    // Util methods

    private Optional<User> findByUsernameWithSecrets(String username) {
        return userRepository.findByUsername(username);
    }
}
