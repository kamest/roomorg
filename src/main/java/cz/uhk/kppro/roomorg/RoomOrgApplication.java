package cz.uhk.kppro.roomorg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootApplication
public class RoomOrgApplication {
	public static void main(String[] args) {
		SpringApplication.run(RoomOrgApplication.class, args);
	}
}
