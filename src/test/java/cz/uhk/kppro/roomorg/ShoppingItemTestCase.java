package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.dao.ShoppingItemRepository;
import cz.uhk.kppro.roomorg.model.ShoppingItem;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ShoppingItemTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private ShoppingItemRepository repository;

	@Test
	public void testShoppingItemEndpoint() {
		init();

		List<ShoppingItem> all = (List<ShoppingItem>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		ShoppingItem one = getRestTemplate().getForObject(getUri("get/1"), ShoppingItem.class);
		Assertions.assertNull(one);

		ShoppingItem createdPojo = getRestTemplate().postForObject(getUri("store"), createExamplePojo(), ShoppingItem.class);
		Assertions.assertNotNull(createdPojo);

		updatePojo(createdPojo);
		ShoppingItem updatedPojo = getRestTemplate().postForObject(getUri("update"), createdPojo, ShoppingItem.class);
		Assertions.assertTrue(updatedPojo != null && updatedPojo.equals(createdPojo));

		all = (List<ShoppingItem>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && !all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), ShoppingItem.class);
		Assertions.assertNotNull(one);

		getRestTemplate().delete(getUri("delete/" + updatedPojo.getId()));

		all = (List<ShoppingItem>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), ShoppingItem.class);
		Assertions.assertNull(one);

		destroy();
	}

	@Override
	protected String endpointPrefix() {
		return "api/shoppingItem";
	}

	public ShoppingItem createExamplePojo() {
		return new ShoppingItem("dddd", 2.0);
	}

	public void updatePojo(ShoppingItem pojo) {
		pojo.setTitle("eee");
		pojo.setQuantity(3.2);
	}

}
