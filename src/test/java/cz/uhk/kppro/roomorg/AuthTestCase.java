package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.dao.EventRepository;
import cz.uhk.kppro.roomorg.model.Event;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.web.AuthenticationController.AuthenticationRequest;
import lombok.Getter;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AuthTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private EventRepository repository;

	@Test
	public void testAuthEndpoint() {
		RestTemplate restTemplate = new RestTemplate();
		final AtomicReference<String> bearer = new AtomicReference<>();
		String username = "test" + new Random().nextInt(500000);
		String password = "test123";

		initRestTemplate(restTemplate, "register", username, password, bearer);

		assertUserInfo(restTemplate, username);

		initRestTemplate(restTemplate, "auth", username, password, bearer);

		assertUserInfo(restTemplate, username);

		username = "test" + new Random().nextInt(500000);
		bearer.set(null);
		initRestTemplate(restTemplate, "register", username, password, bearer);
		assertUserInfo(restTemplate, username);

		restTemplate.getForEntity(getBaseUri() + "api/logout", Object.class);

		String err = null;
		try {
			restTemplate.exchange(getUri("me"), HttpMethod.GET, null, User.class);
		} catch (HttpClientErrorException e) {
			Assertions.assertEquals(e.getStatusCode(), HttpStatus.UNAUTHORIZED);
			err = e.getMessage();
		}
		Assertions.assertNotNull(err);


	}

	private void assertUserInfo(RestTemplate restTemplate, String username) {

		User me = restTemplate.getForObject(getUri("me"), User.class);
		Assertions.assertNotNull(me);
		Assertions.assertNotNull(me.getId());
		Assertions.assertEquals(me.getUsername(), username);
	}

	private void initRestTemplate(RestTemplate restTemplate, String endpoint, String userName, String password, AtomicReference<String> bearer) {

		restTemplate.setInterceptors(Collections.singletonList((request, body, execution) -> {

			if (bearer.get() == null) {

				AuthenticationRequest authenticationRequest = new AuthenticationRequest(userName, password);

				Map<String, String> mapResponseEntity = (Map<String, String>) new RestTemplate().postForObject(getBaseUri() + "api/" + endpoint, authenticationRequest, Map.class);
				assert mapResponseEntity != null;
				bearer.set(mapResponseEntity.get("token"));

			}
			request.getHeaders().add("Authorization", "Bearer " + bearer);

			return execution.execute(request, body);
		}));
	}

	@Before
	public void init() {
		getRepository().deleteAll();
	}

	@After
	public void destroy() {
		getRepository().deleteAll();
	}

	public void updatePojo(Event pojo) {
		pojo.setDescription("eee");
		pojo.setTitle("eee");
	}

	@Override
	protected String endpointPrefix() {
		return "api";
	}
}
