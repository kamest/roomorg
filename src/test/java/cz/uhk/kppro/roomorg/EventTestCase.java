package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.dao.EventRepository;
import cz.uhk.kppro.roomorg.model.Event;
import lombok.Getter;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EventTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private EventRepository repository;

	@Test
	public void testEventEndpoint() {
		init();

		List<Event> all = (List<Event>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		Event one = getRestTemplate().getForObject(getUri("get/1"), Event.class);
		Assertions.assertNull(one);

		Event createdPojo = getRestTemplate().postForObject(getUri("store"), createExamplePojo(), Event.class);
		Assertions.assertNotNull(createdPojo);

		updatePojo(createdPojo);
		Event updatedPojo = getRestTemplate().postForObject(getUri("update"), createdPojo, Event.class);
		Assertions.assertTrue(updatedPojo != null && updatedPojo.equals(createdPojo));

		all = (List<Event>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && !all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), Event.class);
		Assertions.assertNotNull(one);

		getRestTemplate().delete(getUri("delete/" + updatedPojo.getId()));

		all = (List<Event>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), Event.class);
		Assertions.assertNull(one);

		destroy();
	}

	@Before
	public void init() {
		getRepository().deleteAll();
	}

	@After
	public void destroy() {
		getRepository().deleteAll();
	}

	public Event createExamplePojo() {
		return new Event("sss", "dddd");
	}

	public void updatePojo(Event pojo) {
		pojo.setDescription("eee");
		pojo.setTitle("eee");
	}

	@Override
	protected String endpointPrefix() {
		return "api/event";
	}
}
