package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.business.GalleryService;
import cz.uhk.kppro.roomorg.dao.GalleryRepository;
import cz.uhk.kppro.roomorg.dao.MessageRepository;
import cz.uhk.kppro.roomorg.model.Message;
import cz.uhk.kppro.roomorg.model.Photo;
import cz.uhk.kppro.roomorg.model.PhotoWithByteArray;
import cz.uhk.kppro.roomorg.model.PhotoWithMultipartFile;
import cz.uhk.kppro.roomorg.web.GalleryController;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GalleryTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private GalleryRepository repository;

	@Autowired
	@Getter
	private GalleryService service;

	@Autowired
	@Getter
	private GalleryController controller;

	@Test
	public void testGalleryEndpoint() throws IOException {
		init();
		setUser();
		List<Photo> all = service.getList();
		Assertions.assertTrue(all != null && all.isEmpty());

		Photo one = service.getById(1);
		Assertions.assertNull(one);

		String fileName = "aaa.png";
		String testContent = "test";
		Photo createdPojo = service.store(new PhotoWithMultipartFile(createExamplePojo(),null),new ByteArrayInputStream(testContent.getBytes(StandardCharsets.UTF_8)), fileName);
		Assertions.assertNotNull(createdPojo);

		updatePojo(createdPojo);
		Photo updatedPojo = service.store(new PhotoWithMultipartFile(createdPojo,null),new ByteArrayInputStream(testContent.getBytes(StandardCharsets.UTF_8)), fileName);
		Assertions.assertNotNull(updatedPojo);
		Assertions.assertEquals(createdPojo, updatedPojo);

		all = service.getList();
		Assertions.assertTrue(all != null && !all.isEmpty());

		PhotoWithByteArray withFile = service.getById(updatedPojo.getId());
		Assertions.assertNotNull(withFile);
		Assertions.assertEquals(testContent, new String(withFile.getContent(), StandardCharsets.UTF_8));

		controller.delete(updatedPojo.getId());

		all = service.getList();
		Assertions.assertTrue(all != null && all.isEmpty());

		withFile = service.getById(updatedPojo.getId());
		Assertions.assertNull(withFile);

		destroy();
	}

	@Override
	protected String endpointPrefix() {
		return "api/gallery";
	}

	public Photo createExamplePojo() {
		return new Photo("dddd");
	}

	public void updatePojo(Photo pojo) {
		pojo.setTitle("eee");
	}

	private void setUser(){

		SecurityContextHolder.getContext().setAuthentication(new Authentication() {
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				return null;
			}

			@Override
			public Object getCredentials() {
				return null;
			}

			@Override
			public Object getDetails() {
				return null;
			}

			@Override
			public Object getPrincipal() {
				return new User("test","test",true,true,true,true, Collections.emptyList());
			}

			@Override
			public boolean isAuthenticated() {
				return false;
			}

			@Override
			public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

			}

			@Override
			public String getName() {
				return null;
			}
		});
	}

}
