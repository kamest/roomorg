package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.dao.RoomRepository;
import cz.uhk.kppro.roomorg.model.Room;
import cz.uhk.kppro.roomorg.model.User;
import cz.uhk.kppro.roomorg.web.UserinfoController.RoomChangeRequest;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserInfoTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private RoomRepository repository;

	@Test
	public void testShoppingItemEndpoint() {
		init();

		User me = getRestTemplate().getForObject(getUri("me"), User.class);
		Assertions.assertNotNull(me);
		Assertions.assertNotNull(me.getId());


		Room czRoom = new Room("czRoomOrig", "passOfRoom");
		me = getRestTemplate().postForObject(getUri("createRoom"), czRoom, User.class);
		Assertions.assertNotNull(me);
		Assertions.assertNotNull(me.getId());
		Assertions.assertNotNull(me.getRoom());
		Assertions.assertEquals(me.getRoom().getName(), "czRoomOrig");
		Assertions.assertNull(me.getRoom().getSecret());
		Assertions.assertNotNull(me.getRoom().getId());

		Room skRoom = new Room("skRoomOrig", "passOfRooma");
		me = getRestTemplate().postForObject(getUri("createRoom"), skRoom, User.class);
		Assertions.assertNotNull(me);
		Assertions.assertNotNull(me.getId());
		Assertions.assertNotNull(me.getRoom());
		Assertions.assertEquals(me.getRoom().getName(), "skRoomOrig");
		Assertions.assertNull(me.getRoom().getSecret());
		Assertions.assertNotNull(me.getRoom().getId());

		RoomChangeRequest roomChangeRequest = new RoomChangeRequest("czRoomOrig", "passOfRoom");
		me = getRestTemplate().postForObject(getUri("changeRoom"), roomChangeRequest, User.class);
		Assertions.assertNotNull(me);
		Assertions.assertNotNull(me.getId());
		Assertions.assertNotNull(me.getRoom());
		Assertions.assertEquals(me.getRoom().getName(), "czRoomOrig");
		Assertions.assertNull(me.getRoom().getSecret());
		Assertions.assertNotNull(me.getRoom().getId());

		destroy();
	}

	@Override
	protected String endpointPrefix() {
		return "api";
	}

}
