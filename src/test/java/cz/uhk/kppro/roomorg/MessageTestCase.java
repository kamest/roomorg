package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.dao.MessageRepository;
import cz.uhk.kppro.roomorg.model.Message;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MessageTestCase extends AbstractRoomorgTestCase {

	@Autowired
	@Getter
	private MessageRepository repository;

	@Test
	public void testMessageEndpoint() {
		init();

		List<Message> all = (List<Message>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		Message one = getRestTemplate().getForObject(getUri("get/1"), Message.class);
		Assertions.assertTrue(one == null);

		Message createdPojo = getRestTemplate().postForObject(getUri("store"), createExamplePojo(), Message.class);
		Assertions.assertTrue(createdPojo != null);

		updatePojo(createdPojo);
		Message updatedPojo = getRestTemplate().postForObject(getUri("update"), createdPojo, Message.class);
		Assertions.assertTrue(updatedPojo != null && updatedPojo.equals(createdPojo));

		all = (List<Message>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && !all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), Message.class);
		Assertions.assertNotNull(one);

		getRestTemplate().delete(getUri("delete/" + updatedPojo.getId()));

		all = (List<Message>) getRestTemplate().getForObject(getUri("all"), List.class);
		Assertions.assertTrue(all != null && all.isEmpty());

		one = getRestTemplate().getForObject(getUri("get/" + updatedPojo.getId()), Message.class);
		Assertions.assertNull(one);

		destroy();
	}

	@Override
	protected String endpointPrefix() {
		return "api/messages";
	}

	public Message createExamplePojo() {
		return new Message("dddd");
	}

	public void updatePojo(Message pojo) {
		pojo.setContent("eee");
	}

}
