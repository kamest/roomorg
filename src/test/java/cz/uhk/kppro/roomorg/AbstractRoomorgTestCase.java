package cz.uhk.kppro.roomorg;

import cz.uhk.kppro.roomorg.web.AuthenticationController.AuthenticationRequest;
import org.junit.After;
import org.junit.Before;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Random;

/***
 *  No extra information provided - see (selfexplanatory) method signatures.
 *  I have the best intention to write more detailed documentation but if you see this, there was not enough time or will to do so.
 *
 *  @author Stɇvɇn Kamenik (kamenik.stepan@gmail.com), (c) 2021
 **/
public abstract class AbstractRoomorgTestCase {

	private RestTemplate restTemplate;


	@Before
	public void init() {
		getRepository().deleteAll();
	}

	@After
	public void destroy() {
		getRepository().deleteAll();
	}

	@LocalServerPort
	int randomServerPort;

	private String bearerToken;

	protected String getUri(String endpoint) {
		return getBaseUri() + endpointPrefix() + "/" + endpoint;
	}

	String getBaseUri() {
		return "http://localhost:" + randomServerPort + "/";
	}

	protected abstract String endpointPrefix();

	protected abstract JpaRepository getRepository();

	protected RestTemplate getRestTemplate() {
		if (restTemplate == null) {
			restTemplate = new RestTemplate();
		}
		restTemplate.setInterceptors(Collections.singletonList((request, body, execution) -> {
			if (bearerToken == null) {

				AuthenticationRequest authenticationRequest = new AuthenticationRequest("test" + new Random().nextInt(500000), "test123");


				Map<String, String> mapResponseEntity = (Map<String, String>) new RestTemplate().postForObject(getBaseUri() + "api/register", authenticationRequest, Map.class);
				assert mapResponseEntity != null;
				bearerToken = mapResponseEntity.get("token");
			}
			request.getHeaders().add("Authorization", "Bearer " + bearerToken);

			return execution.execute(request, body);
		}));
		return restTemplate;
	}

}
