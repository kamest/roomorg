#!/usr/bin/env bash
export JAVA_HOME=/usr/lib/jvm/jdk-11.0.2+9/
mvn -T 4 clean source:jar install -DskipTests -Dmaven.test.skip=true --fail-at-end
docker rm roomorg .
docker rmi roomorg .
docker build -t roomorg .
docker tag roomorg registry.gitlab.com/kamest/roomorg
docker push registry.gitlab.com/kamest/roomorg:latest
