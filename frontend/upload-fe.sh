#!/usr/bin/env bash
docker rm roomorg/fe .
docker rmi roomorg/fe .
docker build -t roomorg/fe .
docker tag roomorg/fe registry.gitlab.com/kamest/roomorg/fe
docker push registry.gitlab.com/kamest/roomorg/fe:latest
