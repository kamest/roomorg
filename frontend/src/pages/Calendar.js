import React from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import EventModal from '../components/EventModal';
import { Calendar as Cal } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import * as App from "../App";

const paddingFive = {
  padding: '1em',
};

function removeComment(e) {
  e.jsEvent.preventDefault(); // don't let the browser navigate
  const user = JSON.parse(localStorage.getItem('user'));
  const config = {
    headers: {
      'Authorization': `Bearer ${user.token}`}
  };
  let id = e.event.id;
  // odstranění článku
  axios.delete( App.api +"api/event/delete/"+id, config)
      .then(result => {
        if (result.status === 200) {
          loadCalendar();
        } else {
          console.log('result ' + result.status)
        }
      }).catch(e => {
    console.log('result ' + JSON.stringify(e))
  });
}


function loadEvents() {
  return JSON.parse(localStorage.getItem("events"));
}

export let saveEvents = async function () {
  const user = JSON.parse(localStorage.getItem('user'));
  const config = {
    headers: {
      'Authorization': `Bearer ${user.token}`
    }
  };

  await axios.get(App.api +"api/event/all", config)
      .then(result => {
        if (result.status === 200) {
          const value = JSON.stringify(result.data);
          localStorage.setItem("events", value)
        } else {
          console.log('resssssssult ' + result.status)
        }
      }).catch(e => {
        console.log('reaaaaasult ' + JSON.stringify(e))
      });
}

export let loadCalendar = function() {
  saveEvents().then(()=>{
    const calendarEl = document.getElementById('calendar');
    // noinspection JSUnusedGlobalSymbols
    const calendar = new Cal(calendarEl, {
      firstDay: 1,
      locale: "cs-CZ",
      plugins: [dayGridPlugin],
      eventClick: function (info) {
        removeComment(info)
      }
    });

    loadEvents().forEach(i => {


      calendar.addEvent({
        id: i.id,
        title: i.title,
        start: i.eventDate,
        allDay: true
      })

    })

    calendar.render();
  })
}

export class Calendar extends React.Component {

  componentDidMount() {
    loadCalendar();
  }

  render(){
    return (

      <div>
        {/* Hlavní obsah */}
        <div className="section no-pad-bot" id="index-banner">
          <script>loadCalendar()</script>
          {/* Komentáře */}
          <div className="container">
            <div className="row">
              <div className="col s12">

                <div className="right-align" style={paddingFive}>
                  <a href="/#"
                     className="btn-floating  btn-large waves-effect waves-light red modal-trigger"
                     onClick={(e)=>{
                       e.preventDefault()
                       ReactDOM.render(
                           <EventModal/>,
                           document.getElementById("eventModalHolder")
                       );
                     }}>
                    <i className="material-icons">add</i>
                  </a>

                </div>
                <div id="calendar"/>
              </div>
            </div>
          </div>
        {/* util pro vložení CommentModal */}
        <div id="eventModalHolder"/>

      </div>
      </div>

    );
  }
}

export default Calendar;
