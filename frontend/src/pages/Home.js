import React from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import * as App from "../App";
import MessageModal from "../components/MessageModal";

const rightText = {
  textAlign: 'right',
};
const floatRight = {
  width: '80%',
  float: 'right',
};
const floatLeft = {
  width: '80%',
  float: 'left'
};

const lineBreak = {
  whiteSpace: 'pre-line',
  wordBreak: 'break-word'
};

/**
 * Slouží pouze pro výpis článků
 */
class Home extends React.Component {
  state = {
    messages: []
  }


  fetchData() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`,
        'Access-Control-Allow-Headers': `*`,
        'Access-Control-Allow-Origin': `*`
      }
    };

    axios.get(App.api +"api/messages/all",config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          messages: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  render(){
    const { messages } = this.state;

    let user;
    const item = localStorage.getItem('user');
    if(item !== null){
      user=JSON.parse(item);
    }

    //Výpis článků
    return (
      <div className="section no-pad-bot" id="index-banner">
        <div className="container">

            <div className="row">
              {
                messages
                  .slice()
                  .sort((i,j)=> i.created - j.created)
                  .reverse()
                  .map(message => {
                    const { id, content, created, author } = message;

                    return (
                      <div key={id} id={id} className="col s12">
                        <div className="card" style={author.username === user.username? floatRight : floatLeft}>
                          <div className="card-content">
                            <p className="col s8" style={lineBreak}>
                              <span className="card-title activator grey-text text-darken-4">
                                {content}
                              </span>
                            </p>
                            <div className="col s4" style={rightText}>
                              <b>{author.username}</b>
                              <br/><i>{new Date(created).toLocaleDateString()}</i>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })
              }
            </div>
        </div>

        <MessageModal/>

      </div>
    )}
}

export default Home;
