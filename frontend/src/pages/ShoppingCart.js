import React from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import * as App from "../App";
import ShoppingItemModal from "../components/ShoppingItemModal";

const twoPaddingSides = {
  padding: '2em 0',
};
const vAli = {
  verticalAlign: 'super',
  paddingLeft: '0.5em',
};

const paddingFive = {
  padding: '1em',
};


class ShoppingCart extends React.Component {

  state = {
    article: [],
    comments: []
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };

    axios.get(App.api +"api/shoppingItem/all", config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          comments: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }


  removeShoppingItem(e) {
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };
    let nodeId = e.currentTarget.id;
    let id = nodeId.replace("delete_com_","")
    // odstranění článku
    axios.delete(App.api +"api/shoppingItem/delete/"+id, config)
    .then(result => {
      if (result.status !== 200) {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
    window.location.reload();
  }

  render(){

    const { comments } = this.state;
    const user = JSON.parse(localStorage.getItem('user'));
    return (

      <div>
        {/* Hlavní obsah */}
        <div className="section no-pad-bot" id="index-banner">

          {/* Komentáře */}
          <div className="container">
            <div className="row" style={twoPaddingSides}>

              <div className="right-align" style={paddingFive}>
                <a href="/#"
                  className="btn-floating  btn-large waves-effect waves-light red modal-trigger"
                  onClick={(e)=>{
                      e.preventDefault()
                      ReactDOM.render(
                        <ShoppingItemModal/>,
                        document.getElementById("shoppingItemModalHolder")
                      );
                    }}>
                  <i className="material-icons">add</i>
                  </a>

              </div>
              <div className="col s12">
                {
                  comments
                      .slice()
                      .sort((i,j)=> i.created - j.created)
                      .reverse()
                      .map(comment => {
                    let commentAuthorName = comment.name;
                    return(

                      <div className="card blue">
                        <div className="card-content white-text">
                          <p>{comment.title}</p>
                          <p>{comment.quantity}</p>
                        </div>

                          {
                            comment.authorId === user.id?
                            <div className="card-action">
                              <a href="/#/"
                                className="waves-effect waves-light modal-trigger"
                                id={"edit_com_"+comment.id}
                                onClick={(e)=>{
                                    e.preventDefault()
                                    ReactDOM.render(<ShoppingItemModal commentId={comment.id} reload={this.componentDidMount} title={comment.title} quantity={comment.quantity}/>, document.getElementById("shoppingItemModalHolder"));
                                }}
                                >
                                  <i className="material-icons red-text text-lighten-3">
                                    edit
                                  </i>
                              </a>
                              <a href="/#" className="waves-effect waves-light" onClick={this.removeShoppingItem} id={"delete_com_"+comment.id}><i className="material-icons red-text text-lighten-3">delete</i></a>
                              </div> : ""}

                      </div>
                    )
                  })
                }

              </div>
            </div>
          </div>
        {/* util pro vložení CommentModal */}
        <div id="shoppingItemModalHolder"/>

      </div>
      </div>

    );
  }
}

export default ShoppingCart;
