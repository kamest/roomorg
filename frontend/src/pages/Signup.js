import React, {useState} from "react";
import { Redirect } from "react-router-dom";
import axios from 'axios';
import {  Error } from "../components/AuthForms";
import * as App from "../App";

function Signup(props) {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);
  const [name, setName] = useState("");
  const referer = props.location.state  && props.location.state.referer ?  props.location.state.referer : '/home';

  function register(e) {
    e.preventDefault();
    // Vyvoření uživatele a vygenerování tokenu
    axios.post(App.api +"api/register", {
      'password' : password,
      'username' : name
    }).then(result => {
      if (result.status === 200) {
        // uložení tokenu
        localStorage.setItem('user', JSON.stringify(result.data))
        setLoggedIn(true);
        window.location.reload();
      }
    }).catch(e => {
      setIsError(true);
      console.log('result ' + JSON.stringify(e))
    });
  }

  if (isLoggedIn) {
    return <Redirect to={referer} />;
  }
  return (

    <main>
      <center>

        <h5 className="red-text">Register to see more...</h5>

        <div className="container">
          <div className="z-depth-1 grey lighten-4 row signUp">

            <form className="col s12" onSubmit={register}>
              <div className='row'>
                <div className='input-field col s12'>
                  <input
                    className='validate'
                    type='text'
                    name='name'
                    id='name'
                    placeholder="Přezdívka"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input
                    className='validate'
                    type='password'
                    name='password'
                    id='password'
                    placeholder="Heslo"
                    value={password}
                    onChange={e => {
                      setPassword(e.target.value);
                    }} />
                </div>
              </div>

              <br />
              <center>
                <div className='row'>
                  <button className='col s12 btn btn-large waves-effect red'>Registrovat</button>
                  { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </center>
            </form>
          </div>
        </div>
      </center>
    </main>
  );
}

export default Signup;

