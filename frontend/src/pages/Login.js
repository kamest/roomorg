import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import axios from 'axios';
import { Button, Error } from "../components/AuthForms";
import * as App from "../App";

function Login(props) {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [err, setErr] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const referer = props.location.state  && props.location.state.referer ?  props.location.state.referer : '/home';

  function postLogin(e) {
    e.preventDefault();
    // Vygenerování tokenů na serveru
    axios.post(App.api +"api/auth", {
      'username' : userName,
      'password' : password,

    }).then(result => {
      if (result.status === 200) {
        // Uložení do lokální storage
        localStorage.setItem('user', JSON.stringify(result.data))
        setLoggedIn(true);
        window.location.reload(false);
      }
    }).catch(e => {

      if (e.response.status ===401){
        setIsError(true);
        setErr("Údaje nejsou správné, zkoukněte ještě jednou papírek u PC!");
      }else {
        setIsError(true)
        setErr("Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !");
      }
      console.log('result ' + JSON.stringify(e))
    });
  }

  if (isLoggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <main>
      <center>
        <div className="section"/>

        <h5 className="red-text">Prosím, přihlašte se..</h5>
        <div className="section"/>

        <div className="container">
          <div className="z-depth-1 grey lighten-4 row login" >

            <form className="col s12">
              <div className='row'>
                <div className='col s12'>
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input
                      className='validate'
                      type='email'
                      name='email'
                      id='email'
                      placeholder="email"
                      value={userName}
                      onChange={e => {
                        setUserName(e.target.value);
                      }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input
                    className='validate'
                    type='password'
                    name='password'
                    id='password'
                    placeholder="heslo"
                    value={password}
                    onChange={e => {
                      setPassword(e.target.value);
                    }} />
                </div>
              </div>

              <br />
              <center>
                <div className='row'>
                 <Button type='submit' name='btn_login' className='col s12 btn btn-large waves-effect red' onClick={postLogin}>Přihlásit</Button>

                 { isError && <Error>{err}</Error> }
                </div>
              </center>
            </form>
          </div>
        </div>
        <Link to="/signup" className="red-text">Potřebujete založit účet?</Link>
      </center>

      <div className="section"/>
      <div className="section"/>
    </main>
  );
}

export default Login;
