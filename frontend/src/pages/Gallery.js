import React from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import ImageModal from "../components/ImageModal";
import * as App from "../App";

const twoPaddingSides = {
  padding: '2em 0',
};
const cursorPointer = {
  cursor: 'pointer'
};
const imageSize = {
  maxHeight:"5em",
  maxWidth:"70%"
};

class ShoppingCart extends React.Component {

  state = {
    file: null,
    images: []
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };

    axios.get(App.api +"api/gallery/all", config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          images: result.data
        })

      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }


  removeImage(e) {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };
    // odstranění článku
    axios.delete(App.api +"api/gallery/delete/"+e, config)
    .then(result => {
      if (result.status === 200) {
        window.location.reload(false);
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }

  render(){

    const { images } = this.state;
    return (

      <div>
        {/* Hlavní obsah */}
        <div className="section no-pad-bot" id="index-banner">

          {/* Komentáře */}
          <div className="container">
            <div className="row" style={twoPaddingSides}>

              <div className="right-align">
                <a href="/#"
                  className="btn-floating  btn-large waves-effect waves-light red modal-trigger"
                  onClick={(e)=>{
                      e.preventDefault()
                      ReactDOM.render(
                        <ImageModal/>,
                        document.getElementById("imageModalHolder")
                      );
                    }}>
                  <i className="material-icons">add</i>
                  </a>

              </div>
            </div>


            <div className="row">
              {
                images.map(image => {
                  const { id, title, content, created } = image;
                  return (
                      <div key={id} id={id} className="col s12 m6 l3" style={cursorPointer} onClick={(e)=>{
                        e.preventDefault();
                        this.removeImage(id);
                      }}>
                        <div className="card">
                          <div className="card-content">
                            <img src={"data:image/png;base64,"+content} style={imageSize} alt={title}/>
                            <span className="card-title activator grey-text text-darken-4">
                              {title}
                            </span>
                            <p>{new Date(created).toLocaleDateString()}</p>
                          </div>
                        </div>
                      </div>
                  );
                },null)
              }
            </div>

          </div>
        {/* util pro vložení CommentModal */}
        <div id="imageModalHolder"/>

      </div>
      </div>

    );
  }
}

export default ShoppingCart;
