import React, { useState } from "react";
import axios from 'axios';
import { Error } from "./AuthForms";
import FilePicker  from '@mavedev/react-file-picker';
import * as App from "../App";

const modalStyle={
  zIndex: "1003",
  display: "block",
  opacity: "1",
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}
const displayNone = {
  display: "none"
}


/**
 * Slouží pro editaci komentářů v popupu
 */
function ImageModal(props) {

  const [title, setTitle] = useState(props.title);
  const [file, setFile] = useState(props.file);
  const [isError, setIsError] = useState(false);


  function sendComment(e) {
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`,
        'Content-Type': `multipart/form-data`
      }
    };

    let formData = new FormData();
    formData.set('title', title);
    formData.set('multipartFile', file);
    // Vytvoření
    axios.post(App.api +"api/gallery/store", formData, config).then(result => {
      if (result.status === 200) {
        window.location.reload(false);
      } else {
        setIsError(true);
      }
    }).catch(e => {
      setIsError(true);
      console.log('result ' + JSON.stringify(e))
    });

  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>{props.commentId? "Úprava" : "Vytvoření"} komentáře</h5>
          <div className="row">
            <form className="col s12" >
              <div className="row">
                <div className="input-field col s12">
                  <textarea
                    id="textarea1"
                    className="materialize-textarea"
                    value={title}
                    onChange={e => {
                      setTitle(e.target.value);
                    }} />
                  <FilePicker
                      maxSize={12}
                      sizeUnit='MB'
                      extensions={['.jpg', '.png','.jpeg']}
                      onFilePicked={(file) => { setFile(file)}}
                      onSuccess={() => { document.getElementById('sendImage').style.display="block" }}
                      onError={(code) => { console.log(code); }}
                  >
                    <button type='button'>Nahrát obrázek</button>
                  </FilePicker>

                  <label htmlFor="textarea1">Text komentáře</label>
                </div>
                <div className="input-field col s12">
                  <button id="sendImage" style={displayNone} className="btn waves-effect waves-light red" type="submit" name="action" onClick={sendComment}>Odeslat
                    <i className="material-icons right">send</i>
                  </button>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-overlay" style={modalOverlay}/>
    </div>
  );
}

export default ImageModal;
