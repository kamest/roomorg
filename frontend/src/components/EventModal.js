import React, { useState } from "react";
import axios from 'axios';
import { Error } from "./AuthForms";
import ReactDOM from 'react-dom';
import DatePicker from 'react-date-picker';
import {loadCalendar} from "../pages/Calendar";
import * as App from "../App";

const modalStyle={
  zIndex: "1003",
  display: "block",
  opacity: "1",
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}
let negativeZIndex = {
  zIndex: "-100",
}

/**
 * Slouží pro editaci komentářů v popupu
 */
function EventModal(props) {

  const [title, setTitle] = useState(props.title);
  const [eventDate, setEventDate] = useState(new Date());
  const [isError, setIsError] = useState(false);


  async function sendComment(e) {
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`
      }
    };
    // Vytvoření
    eventDate.setTime(eventDate.getTime() - eventDate.getTimezoneOffset() * 60 * 1000);
    axios.post(App.api +"api/event/store", {
      'title': title,
      'eventDate': eventDate,
    }, config).then(result => {
      if (result.status === 200) {

        loadCalendar();

      } else {
        setIsError(true);
      }
    }).catch(e => {
      setIsError(true);
      console.log('result ' + JSON.stringify(e))
    });

    ReactDOM.unmountComponentAtNode(document.getElementById("eventModalHolder"));

  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>Vytvoření komentáře</h5>
          <div className="row">
            <form className="col s12" >
              <div className="row">
                <div className="input-field col s12">
                  {/*<label htmlFor="title">Název události</label>*/}
                  <input
                    id="title"
                    type="text"
                    className="materialize-textarea"
                    value={title}
                    onChange={e => {
                      setTitle(e.target.value);
                    }} />
                  <label htmlFor="title">Název události</label>
                  <p><small>Datum události:</small></p>
                  <DatePicker
                      id="eventDate"
                      onChange={setEventDate}
                      value={eventDate}
                      locale="cs-CZ"
                  />
                </div>
                <div className="input-field col s12">
                  <a  href={"/calendar?n=" + new Date().getTime()} onClick={sendComment}>
                    <button style={negativeZIndex} className="btn waves-effect waves-light red" type="submit" name="action" >
                      Odeslat
                      <i className="material-icons right">send</i>
                    </button>
                  </a>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-overlay" style={modalOverlay} onClick={()=>ReactDOM.unmountComponentAtNode(document.getElementById("eventModalHolder"))}/>
    </div>
  );
}

export default EventModal;
