import React from "react";

const fixedFooter = {
    position: 'fixed',
    bottom: '0',
    width: '100%',
    textAlign: 'center',
    zIndex: '1200'
}

function Footer() {
return (
<footer className="page-footer red" style={fixedFooter}>
  <div className=" footer-copyright">
    <div className="container">
        Vytvořeno Jakubem Slavíčkem a Štěpánem Kameníkem jako semestrální práce v předmětu KPPRO na UHK.
    </div>
  </div>
</footer>
);
}

export default Footer;
