import React, { useState } from "react";
import axios from 'axios';
import { Error } from "./AuthForms";
import * as App from "../App";

const modalStyle={
  zIndex: "1003",
  display: "block",
  opacity: "1",
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}

/**
 * Slouží pro editaci článků
 */
function MessageModal(props) {

  const [content, setContent] = useState(props.content);
  const [isError, setIsError] = useState(false);


  function sendComment(e){
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };
    let messId = props.aId;
    if(messId == null){
      // odeslání článku jako nového
      axios.post(App.api +"api/messages/store", {
        'content' : content
      },config).then(result => {
        if (result.status === 200) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }else{
      // editace článku
      axios.patch(App.api +"api/messages/update/"+messId, {
        'id' : messId,
        'authorId' : props.authorId,
        'content' : content,
        'created' : props.created
      },config).then(result => {
        if (result.status === 200 ) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }
  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>Vytvoření zprávy</h5>
          <div className="row">
            <form className="col s12" encType="multipart/form-data">
              <div className="row">
                <div className="input-field col s12">
                  <textarea
                    id="textarea1"
                    className="materialize-textarea"
                    value={content}
                    onChange={e => {
                      setContent(e.target.value);
                    }} />
                  <label htmlFor="textarea1">Obsah</label>
                </div>
                <div className="input-field col s12">
                  <button className="btn waves-effect waves-light red" type="submit" name="action" onClick={sendComment}>Odeslat
                    <i className="material-icons right">send</i>
                  </button>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MessageModal;
