import React, { useState } from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Error } from "./AuthForms";
import * as App from "../App";

const modalStyle={
  zIndex: "1003",
  display: "block",
  opacity: "1",
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}

/**
 * Slouží pro editaci komentářů v popupu
 */
function ShoppingItemModal(props) {

  const [title, setTitle] = useState(props.title);
  const [quantity, setQuantity] = useState(props.quantity);
  const [isError, setIsError] = useState(false);


  function sendComment(e){
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };
    let commentId = props.commentId;
    if(commentId == null){
      // Vytvoření
      axios.post(App.api +"api/shoppingItem/store", {
        'title' : title,
        'quantity' : quantity,
      },config).then(result => {
        if (result.status === 200) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }else{
      // Editace
      axios.post(App.api +"api/shoppingItem/update/", {
        'id' : commentId,
        'title' : title,
        'quantity' : quantity,
      },config).then(result => {
        if (result.status === 200) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }
  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>{props.commentId? "Úprava" : "Vytvoření"} položky</h5>
          <div className="row">
            <form className="col s12" >
              <div className="row">
                <div className="input-field col s12">
                  <textarea
                    id="textarea1"
                    className="materialize-textarea"
                    value={title}
                    onChange={e => {
                      setTitle(e.target.value);
                    }} />
                  <input
                      id="quantity"
                      type="number"
                      value={quantity}
                      onChange={e => {
                        setQuantity(e.target.value);
                      }} />

                  <label htmlFor="textarea1">Text komentáře</label>
                </div>
                <div className="input-field col s12">
                  <button className="btn waves-effect waves-light red" type="submit" name="action" onClick={sendComment}>Odeslat
                    <i className="material-icons right">send</i>
                  </button>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-overlay" style={modalOverlay} onClick={()=>ReactDOM.unmountComponentAtNode(document.getElementById("shoppingItemModalHolder"))}/>
    </div>
  );
}

export default ShoppingItemModal;
