// import React from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import React from "react";
import * as App from "../App";

function Header(props) {

  function logOut() {

    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: {
        'Authorization': `Bearer ${user.token}`}
    };
    // Vymazání tokenu na serveru
    axios.post(App.api +"api/logout/", user,config)
    .then(result => {
      if (result.status !== 200) {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });

    // Vymazání tokenu a objektu uživatele v lokální storage
    localStorage.setItem('user',null)

    window.location.reload(false);
  }

  let user = null;
  const item = localStorage.getItem('user');
  if(item !== null){
    const optUser = JSON.parse(item);
    if(optUser != null && optUser.token) user=optUser;
  }

  return (
    <nav className="red lighten-1" role="navigation">
      <div className="nav-wrapper container"><Link id="logo-container"  to="/home" className="brand-logo">RoomOrg</Link>
        <ul className="right hide-on-med-and-down">
          <li><Link to="/home">Home</Link></li>
          <li><Link to="/shoppingCart">Nákupní košík</Link></li>
          <li><Link to="/gallery">Galerie</Link></li>
          <li><Link to="/calendar">Kalendář</Link></li>
          {user ? (
              <span>
                <li><Link to="/home" onClick={logOut}>Logout ({user.username})</Link></li>
              </span>
            ) : (
                user ? <li><Link to="/home" onClick={logOut}>Logout ({user.username})</Link></li> :
              <li><Link to={{ pathname: "/login", state: { referer: props.location } }}>Login</Link></li>
            )}
        </ul>

        {/* pro mobilí telefony */}
        <ul id="nav-mobile" className="sidenav">
          <li><Link to="/home">Home</Link></li>
          <li><Link to="/shoppingCart">Nákupní košík</Link></li>
          <li><Link to="/gallery">Galerie</Link></li>
          <li><Link to="/calendar">Kalendář</Link></li>
          {user ? (
              <span>
                <li><Link to="/home" onClick={logOut}>Logout ({user.username})</Link></li>
              </span>
            ) : (
                user ? <li><Link to="/home" onClick={logOut}>Logout ({user.username})</Link></li> :
              <li><Link to={{ pathname: "/login", state: { referer: props.location } }}>Login</Link></li>
            )}
        </ul>
        <a href="/#" data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
      </div>
    </nav>
  );
}

export default Header;
