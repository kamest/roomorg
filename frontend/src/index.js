import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import axios from 'axios';

axios.interceptors.response.use(response => {
    // Edit response config
    return response;
}, (error) => {
    if (error!= null && error.response.status === 401) {
        localStorage.setItem('user',null)
        App.push({ // here is the redirect component OR USE window.location.href='/login'
            path: '/login',
            name: 'login'
        })
    }
});

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
