import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import PrivateRoute from './PrivateRoute';
import Home from "./pages/Home";
import Header from "./components/Header";
import Login from "./pages/Login";
import Signup from './pages/Signup';
import ShoppingCart from './pages/ShoppingCart';
import Footer from './components/Footer';
import { Calendar } from "./pages/Calendar";
import Gallery from "./pages/Gallery";

export const api = process.env.apiUrl  == null ? "https://roomorg.kamest.eu/" : process.env.apiUrl;

function App() {

  return (
    <Router>
      <div>
        <Header/>

        <Switch>

          {/* Výpis článků */}
          <PrivateRoute exact path="/home" component={Home} />
          <PrivateRoute exact path="/" component={Home} />
          {/* Výpis článku a komentářů */}
          <PrivateRoute path="/shoppingCart" component={ShoppingCart} />
          <PrivateRoute path="/gallery" component={Gallery} />
          <PrivateRoute path="/calendar" component={Calendar} />
          {/* Přihlášní */}
          <Route path="/login" component={Login} />
          {/* Registrace */}
          <Route path="/signup" component={Signup} />
          {/* Fallback */}
          <Redirect to='/' />
        </Switch>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
