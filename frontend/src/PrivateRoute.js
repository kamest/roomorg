import React from "react";
import { Route, Redirect } from "react-router-dom";

function PrivateRoute({ component: Component, ...rest }) {
  let isLogged = false;
  const item = localStorage.getItem('user');
  if(item){
    const user = JSON.parse(item);
    if(user && user.token) isLogged=true;
  }
  // Route pouze pro přihlášené 
  return (
    <Route
      {...rest}
      render={props =>
        isLogged ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { referer: props.location } }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;