FROM adoptopenjdk/openjdk11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /
ENTRYPOINT ["java","-jar","/roomorg-0.0.1-SNAPSHOT.jar"]
